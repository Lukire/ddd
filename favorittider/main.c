#include <stdio.h>
#include <stdlib.h>

int ft = 0;
int smartfavor(int hour, int min) {

    if (min < 10) {
        return 0;
    }
    if (hour == 10) {
        return 0;
    }else if (hour == 11)
    {
        return min == 11? 1:0;
    }else if(hour == 12)
    {
        return (min == 48 || min == 34)? 1:0;
    }

    float d1 = (min/10)%10;
    float d2 = (min%10);

    if ((float )d2 / d1 == (float)d1 / hour) {
        if ((d2 && d1) != 0) {
            return 1;
        }
    }
    
    if (d2 - d1 == d1 - hour)
    {
        return 1;
    }
    return 0;

}

int main() {
    int minutes;
    scanf("%d", &minutes);

    int iter = minutes % 720;
    int hour = 0;
    for (int i = 0; i <= iter; i++) {
        if (i % 60 == 0)
        {
            hour = (((i/60)+12)%12);
            if (hour == 0)
            {
                hour = 12;
            }else if (hour == 10)
            {
                i+=59;
            }


        }

        int min = i%60;
        ft += smartfavor(hour, min);
    }
    ft += minutes/720 * 38;
    printf("%d", ft);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include<stdio.h> 


//Stupid slow sort but whatever
void sort(int a[], int size) {
    for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++)            
		{
			if (a[j] < a[i])
			{
				int tmp = a[i];    
				a[i] = a[j];
				a[j] = tmp;
			}
		}
	}
}

int getRowSum(int a[], int size) {
    int s = 0;
    for (int i = 0; i < size; i++) {
        s += a[i];
    }
    return s;
}

int main() {

    // Getting inputs
    int x, y;
    scanf("%d", &x);
    scanf("%d", &y);
    x-=1;
    y-=1;

    int xc[x];
    int yc[y];
    for (int i = 0; i < x; i++)
    {
        int num;
        scanf("%d", &num);
        xc[i] = num;
    }

    for (int i = 0; i < y; i++)
    {
        int num;
        scanf("%d", &num);
        yc[i] = num;
    }

    //Calling our slow (but quick to write) sorting algorithm
    sort(yc, y);
    sort(xc, x);
    
    //Getting size
    int xsize = getRowSum(xc, x);
    int ysize = getRowSum(yc, y);
    
    int result = 0;
    for (int xi = 0; xi < x; xi++) {
        int xv = xc[xi];
        if (xv == -1) {
            continue;
        }
        
        for (int yi = 0; yi < y; yi++) {
            int yv = yc[yi];

            if (yv == -1) {
                continue;
            }
            if(yv > xv) {
                result +=yv + xsize;
                ysize -=yv;
                yc[yi] = (int) -1;
            }else if (xv > yv) {
                result +=xv + ysize;
                xsize -=xv;
                xc[xi] = (int) -1;
                break;
            }else{
                if (ysize > xsize) {
                    result +=yv + xsize;
                    ysize -=yv;
                    yc[yi] = (int) -1;
                }else{
                    result +=xv + ysize;
                    xsize -=xv;
                    xc[xi] = (int) -1;
                    break;
                }
            }
        }
    }


    //Stupid check, but since there's no time limit I won't bother
    //to actually make the stuff nice
    for (int i = 0; i < x; i++)
    {
        if (xc[i] != -1) {
            result += xc[i];
        }
    }

    for (int i = 0; i < y; i++)
    {
        if (yc[i] != -1) {
            result += yc[i];
        }
    }

    printf("%d",result);

    return 0;
}


